import fr from '../locales/fr'
import en from '../locales/en'

export default {
  defaultLocale: 'fr',
  locales: [
    {
      code: 'en',
      name: 'English',
    },
    {
      code: 'fr',
      name: 'Français',
    },
  ],
  strategy: 'no_prefix',
  vueI18n: {
    locale: 'fr',
    fallbackLocale: 'fr',
    messages: {
      en,
      fr,
    },
    dateTimeFormats: {
      en: {
        short: {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        },
        long: {
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          weekday: 'short',
          hour: 'numeric',
          minute: 'numeric',
        },
      },
      fr: {
        short: {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        },
        long: {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          weekday: 'short',
          hour: 'numeric',
          minute: 'numeric',
        },
      },
    },
  },
}
