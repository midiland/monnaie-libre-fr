import Vue from 'vue'

// v-focus
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted(el, binding) {
    if (!('value' in binding) || binding.value) el.children[0].focus()
  },
})
