import i18n from './plugins/i18n.js'

export default {
  target: 'static',

  /**
   * Global page headers (https://go.nuxtjs.dev/config-head)
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }],
    bodyAttrs: {
      class: process.env.NODE_ENV === 'development' ? 'debug-screens' : '',
    },
  },

  /**
   * Customize the progress-bar color
   */
  loading: { color: '#7c3aed' },

  /**
   * Global CSS (https://go.nuxtjs.dev/config-css)
   */
  css: [],

  /**
   * Auto import components (https://go.nuxtjs.dev/config-components)
   */
  components: true,

  /**
   * Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
   */
  plugins: ['~plugins/vue-tailwind', '~/plugins/directives.client.js'],

  /**
   * Modules for dev and build (https://go.nuxtjs.dev/config-modules)
   */
  buildModules: [
    // https://composition-api.nuxtjs.org/
    // TODO: remove it when nuxt3 released
    '@nuxtjs/composition-api',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
    // https://color-mode.nuxtjs.org/
    '@nuxtjs/color-mode',
    // https://github.com/nuxt-community/fontawesome-module
    [
      '@nuxtjs/fontawesome',
      {
        component: 'fa', // component name
        addCss: false,
        icons: {
          solid: [
            'faHome',
            'faSearch',
            'faSkullCrossbones',
            'faChevronLeft',
            'faChevronRight',
            'faArrowLeft',
            'faArrowRight',
            'faSun',
            'faMoon',
            'faHeart',
            'faInfoCircle',
            'faCheckCircle',
            'faExclamationCircle',
            'faExclamationTriangle',
            'faExternalLinkAlt',
            'faBars',
            'faUserCircle',
            'faGlobe',
            'faAngry',
            'faCompressArrowsAlt',
            'faExpandArrowsAlt',
          ],
          brands: [
            'faCreativeCommonsNcEu',
            'faDiaspora',
            'faDiscourse',
            'faGithub',
            'faGitlab',
            'faYoutube',
            'faRocketchat',
            'faFacebook',
            'faTwitter',
            'faDiscord',
            'faApple',
            'faAndroid',
            'faBitcoin',
            'faChrome',
            'faFirefox',
            'faCodepen',
            'faDev',
            'faDocker',
            'faDropbox',
            'faMastodon',
            'faMedium',
            'faNpm',
            'faReddit',
            'faSlack',
            'faSoundcloud',
            'faSpotify',
            'faSteam',
            'faTeamspeak',
            'faTelegram',
            'faTrello',
            'faVimeo',
            'faWhatsapp',
            'faWikipediaW',
            'faWordpress',
          ],
        },
      },
    ],
  ],

  /**
   * Nuxt.js modules (https://go.nuxtjs.dev/config-modules)
   */
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://pwa.nuxtjs.org
    '@nuxtjs/pwa',
    // https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // https://content.nuxtjs.org/fr
    '@nuxt/content',
    // https://i18n.nuxtjs.org
    ['nuxt-i18n', i18n],
    // https://github.com/Chantouch/nuxt-clipboard
    'nuxt-clipboard',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // https://pwa.nuxtjs.org/manifest
  pwa: {
    meta: {
      name: 'Monnaie Libre',
      lang: 'fr',
      theme_color: '#5b21b6',
      ogHost: 'monnaie-libre.fr',
      twitterCard: 'summary_large_image',
      twitterSite: '@monnaie_libre',
      twitterCreator: '@monnaie_libre',
    },
    manifest: {
      name: 'Monnaie Libre',
      short_name: 'monnaie-libre.fr',
      lang: 'fr',
    },
    icon: {
      fileName: 'icon-app.png',
    },
  },

  // https://github.com/Chantouch/nuxt-clipboard
  clipboard: {
    autoSetContainer: true,
  },

  // https://content.nuxtjs.org/fr/configuration
  content: {
    markdown: {
      remarkPlugins: ['remark-breaks'],
    },
  },

  // https://color-mode.nuxtjs.org
  colorMode: {
    preference: 'system', // default value of $colorMode.preference
    fallback: 'light', // fallback value if not system preference found
    classSuffix: '',
  },

  /**
   * Nuxt hooks
   * https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-hooks
   */
  hooks: {
    // Netlifycms cannot create array in json file at root level and nuxt-content need an array. So, ressources.json is parsed to fetch 'ressources' key.
    'content:file:beforeParse': (file) => {
      if (file.extension !== '.json' || !/.*ressources.json$/.test(file.path))
        return
      file.data = JSON.stringify(JSON.parse(file.data).ressources)
    },
  },

  /**
   * Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
