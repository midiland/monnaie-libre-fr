## Site [monnaie-libre.fr](https://monnaie-libre.fr)

Lien provisoire : https://monnaie-libre-fr.vercel.app/

> Ceci est le dépôt du générateur du site.
> Pour éditer son contenu, passez par l'admin du site, ou modifier directement les fichiers dans le dépôt du contenu.

### Build Setup

```bash
## install dependencies
$ yarn install

## serve with hot reload at localhost:3000
$ yarn dev

## generate static project
$ yarn generate

## launch localy
$ yarn start
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Le back office (netlify)

Ici pour l'instant : https://monnaie-libre-fr.vercel.app/

### Lancer le back office (netlify)

     yarn admin

Puis aller sur http://localhost:3000/admin

### Fichier de configuration

     - `static/admin/config.yml` Paramètres de netlify
     - `static/admin/index.html` HTML de la page d'administration de netlify

## Design

### Storybook

Idée abandonnée. J'ai installé, configuré et testé. Mais c'est compliqué, et finalement pas super intégré pour vue.js...

### Afficher toutes les `class` de tailwindcss

     http://localhost:3000/_tailwind/

Sur cette page, vous pouvez copier une class css en cliquant sur le nom, ou maj+click pour copier plusieurs class.

### Icônes

https://fontawesome.com/icons?m=free

## Docs

- @nuxt/content https://content.nuxtjs.org/fr et https://nuxtjs.org/docs/2.x/directory-structure/content/
- @nuxt/svg https://github.com/nuxt-community/svg-module
- @nuxt/composition-api https://composition-api.nuxtjs.org/

Voir aussi`nuxt.config.js` pour la doc d'autres modules...

## Tailwindcss et Vue

https://tailwindcss.com/
https://play.tailwindcss.com/

### Templates et ressources

https://vuetailwind.com/
https://tailblocks.cc/
https://tailwindcomponents.com/
https://www.tailwindtoolbox.com/
https://mertjf.github.io/tailblocks/
http://blocks.wickedtemplates.com/
https://www.creative-tim.com/learning-lab/tailwind-starter-kit/documentation/download

### Divers

https://maizzle.com
https://vuetensils.stegosource.com/introduction.html

## Generate fake data

Modifier le fichier `test/generate-md-files.js` puis :

     node test/generate-md-files.js

Aide faker.js: https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html

# Docker

**Run the nuxt build site in a docker environment**

If you don't got it, run the following command to get the Nginx Docker image:

    docker pull nginx

This downloads all the necessary components for the container. Docker will cache these, so when we run the container we don’t need to download the nginx image(s) each time.

Then start our Nginx Docker container with this command:

    dk run --rm -d -v $(pwd)/dist:/usr/share/nginx/html -p 80:80 nginx:alpine

Go to [](http://localhost/) !

You can change the port. In the command above, replace `80:80` by `4000:80` and go to [](http://localhost:4000/)

# CI Gitlab

https://blog.logrocket.com/how-to-auto-deploy-a-vue-application-using-gitlab-ci-cd-on-ubuntu/
