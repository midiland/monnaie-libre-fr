export default {
  cancel: 'Annuler',
  search: 'Rechercher',
  goBack: 'Retour',

  noResult: {
    text: 'Rien ne correspond à votre recherche...',
    searchWholeSite: 'Chercher "{query}" sur tout le site',
    searchOnForum: 'Chercher "{query}" sur le forum',
  },
  lexique: {
    title: 'Lexique',
    searchPlaceholder: 'Rechercher un terme dans le lexique...',
    tooltipReadmore: 'Cliquez pour en savoir plus...',
  },
  faq: {
    searchPlaceholder: 'Rechercher dans la faq...',
    submitQuestion: 'Proposez votre question !',
    blockSubmitQuestionTitle: 'Pas de réponse à votre question ?',
    blockSubmitQuestionButton: 'Demandez !',
  },
  home: {
    title: 'Accueil',
    searchSitePlaceholder: '...sur le site, blog, faq, lexique...',
    searchRessourcesPlaceholder: '...un site web, une app, une vidéo...',
    searchG1Placeholder: '...un portefeuille, un block...',
  },
  page: {
    updatedAt: 'Modifié le',
    editAdmin: 'Modifier la page',
    editVscode: 'Modifier avec mon éditeur de code',
    toc: 'Table des matières',
    related: 'Voir aussi',
  },
  recherche: {
    title: 'Recherche',
    searchPlaceholder: 'Rechercher sur le site...',
    forumResultTitle: 'Résultats sur le forum',
    noForumResult: '...et rien sur le forum !',
  },
  blog: {
    title: 'Blog',
    searchPlaceholder: 'Rechercher un article de blog...',
  },
  ressources: {
    title: 'Ressources',
    searchPlaceholder: 'Rechercher une ressource...',
    submitResources: 'Proposer une ressource',
  },
}
