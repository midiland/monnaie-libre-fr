export default {
  cancel: 'Cancel',
  search: 'Search',
  goBack: 'Go back',

  noResult: {
    text: 'Nothing matches your search...',
    searchWholeSite: 'Search "{query}" on the whole site',
    searchOnForum: 'Search "{query}" on the forum',
  },
  lexique: {
    title: 'Lexique',
    searchPlaceholder: 'Search a term in the lexique...',
    tooltipReadmore: 'Click to read more...',
  },
  faq: {
    searchPlaceholder: 'Search in faq...',
    submitQuestion: 'Submit your question!',
    blockSubmitQuestionTitle: 'No answer to your question?',
    blockSubmitQuestionButton: 'Ask us !',
  },
  home: {
    title: 'Home',
    searchSitePlaceholder: '...on this site, blog, faq, glossary..',
    searchRessourcesPlaceholder: '...a website, an app, a video...',
    searchG1Placeholder: '...a wallet, an hash...',
  },
  page: {
    updatedAt: 'Updated at',
    editAdmin: 'Edit this page',
    editVscode: 'Edit with my code editor',
    toc: 'Table of content',
    related: 'See also',
  },
  recherche: {
    title: 'Search',
    searchPlaceholder: 'Search on the site...',
    forumResultTitle: 'Results on the forum',
    noForumResult: '...an nothing in the forum !',
  },
  blog: {
    title: 'Blog',
    searchPlaceholder: 'search a blog post...',
  },
  ressources: {
    title: 'Ressources',
    searchPlaceholder: 'Search a ressource...',
    submitResources: 'Submit a ressource',
  },
}
