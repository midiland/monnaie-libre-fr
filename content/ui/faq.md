---
title: Foire aux questions
---
## Des groupes locaux pour poser vos questions de vive voix
N’hésitez pas à vous rapprocher de votre groupe local pour discuter monnaie libre avec des gens qui utilisent déjà la Ğ1 :
### [Voir la carte des groupes locaux](https://framacarte.org/fr/map/monnaie-libre-g1_8702#5/46.104/5.339)

Un plateforme de discussion pour poser vos questions en ligne
Si vous avez besoin de davantage d’explications, il existe une plateforme de discussion à l’intérieur de laquelle vous pouvez poser vos questions :
### [Visiter la plateforme de discussion](https://forum.monnaie-libre.fr/)

Attention : si la plupart des membres de la plateforme vous répondront de façon cordiale, les membres les plus actifs sont parfois aussi les plus agacés de répondre souvent aux mêmes questions. Pensez donc à utiliser [la fonction de recherche](https://forum.monnaie-libre.fr/search), car il y a fort à parier que votre question ait déjà été posée.