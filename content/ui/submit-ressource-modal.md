---
title: Proposez une ressource
inputTitle: null
inputPrecision: null
---

Il manque un lien ? Proposez le !

<a href="/admin/#/collections/ressources/new" target="_blank">C'est par là</a>
