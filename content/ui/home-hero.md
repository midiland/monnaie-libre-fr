<h1 class="mb-4 text-3xl md:text-7xl bg-clip-text text-transparent bg-gradient-to-r from-purple-800 to-blue-600 font-extrabold leading-tight slide-in-bottom-h1">
La monnaie libre
</h1>

<p class="leading-normal text-base text-gray-700 dark:text-gray-300 md:text-5xl mb-8 slide-in-bottom-subtitle">
Repenser la création monétaire...<br />et l'expérimenter !
</p>

<p class="leading-normal text-base text-gray-700 dark:text-gray-300 md:text-2xl mb-6 slide-in-bottom-subtitle">
Un modèle économique garantissant plus de liberté&nbsp;🕊,
d'égalité&nbsp;⚖️&nbsp;et de fraternité&nbsp;🤝 ; c'est possible !
  <nuxt-link
    to="/decouvrir"
    class="block hover:underline text-red-600 font-semibold mt-1"
  >
    <fa icon="arrow-right"></fa><span class="ml-2">Découvrir comment...</span>
  </nuxt-link>
</p>

<p class="leading-normal text-base text-gray-700 dark:text-gray-300 md:text-2xl mb-8 slide-in-bottom-subtitle">
Une expérience citoyenne, solidaire... et subversive&nbsp;✊
  <nuxt-link
    to="/contribuer"
    class="link-contribute block hover:underline text-red-600 font-semibold mt-1"
  >
    <fa icon="arrow-right"></fa><span class="ml-2">Je participe !</span>
  </nuxt-link>
</p>
