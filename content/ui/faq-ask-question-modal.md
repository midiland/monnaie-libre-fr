---
title: Posez une question
inputTitle: null
inputPrecision: null
---

Vous ne trouvez pas de réponses à votre question dans la FAQ ?
Proposez votre question !

<t-input-group label="Votre question" description="Intitulé de la question tel que vous auriez aimé la trouver." class="mt-3">
    <t-input v-model="inputTitle" placeholder="Titre de la question"></t-input>
</t-input-group>

<t-input-group label="Complément" description="Indiquez ici une précision sur la question, une réponse possible, des ressources pour y répondre..." class="my-3">
    <t-textarea v-model="inputPrecision" placeholder="Informations complémentaires..."></t-textarea>
</t-input-group>

Merci pour votre avis !
Nous nous efforcerons de répondre le plus rapidement possible...
