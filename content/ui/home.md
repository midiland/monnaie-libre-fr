<alert type="info">[Page de test](/test)</alert>

### Préambule
Les personnes qui créent la monnaie sont avantagées sur celles qui ne le font pas, car les seconds, pour se procurer la monnaie dont ils ont besoin, doivent travailler pour ceux qui en ont afin d’en obtenir. Les personnes qui créent la monnaie sont donc privilégiés et peuvent ainsi contrôler les autres.
Dans les systèmes de monnaie dette comme l’euro, la monnaie est créée par crédit bancaire. Et oui, contrairement à une croyance très répandue, les banques ne prêtent pas de la monnaie qu’elles possèdent, mais créent ex-nihilo (à partir de rien) de la nouvelle monnaie pour l’occasion. Cette monnaie est détruite lorsque le capital emprunté est remboursé. Il y a donc asservissement du plus grand nombre par la dette, enrichissement d’une minorité par l’intérêt perçu (qui est bel et bien empoché), et un risque permanent de raréfaction ou même disparition de la monnaie si l’ensemble des dettes étaient remboursées.

### Une MONNAIE LIBRE
La Ğ1 (la "June") est la première MONNAIE LIBRE de l'histoire de l'humanité.
Conformément à la Théorie Relative de la Monnaie (TRM) écrite en 2010 par Stéphane Laborde, elle est co-créée sans dette, et à part égale, entre tous les êtres humains de toutes les générations présentes et à venir, sous la forme d'un "paquet" de monnaie, une quantité de Ğ1 (une quantité de "junes"), appelée DIVIDENDE UNIVERSEL (DU).

Je précise tout de suite que, comme les enfants sont des êtres humains à part entière, ils participent à la création monétaire en créant tous les jours leur part de monnaie !

### Un REVENU DE BASE ?...
Pas besoin de se torturer l'esprit pour se demander où, et auprès de qui, collecter de la monnaie à redistribuer pour financer un revenu de base. Le DU de la monnaie libre EST un revenu de base, créé directement sur le compte de chacun-e. C'est tellement plus simple !

### Réduction des inégalités...
Propriété intéressante de ce mode de création monétaire : tous les comptes créateurs de nouvelle monnaie rejoignent la moyenne avec le temps. Ainsi, le solde relatif des riches diminue pendant que celui des pauvres augmente. Cela correspond à prendre aux riches pour donner aux pauvres, mais au lieu de prendre à qui que ce soit, on donne à tous ! C'est pas magique, ça ?

### Une UNITÉ DE MESURE INVARIANTE de la valeur
La Ğ1 est la première monnaie à assurer correctement sa fonction d'unité de compte.
En effet, son DU est une unité de mesure INVARIANTE de la valeur.
De la même manière qu'on ne mesure pas les longueurs avec un mètre élastique pour construire une maison, il serait souhaitable, pour nos échanges économiques, et pour étudier les différents phénomènes de l'économie, d'avoir une unité de mesure qui soit stable, et donc invariante, en tout lieu et en tous temps. Mais est-ce le cas pour la monnaie que nous utilisons tous les jours ? Et bien non !
Toutes les monnaies actuellement en circulation provoquent une instabilité des prix, car la valeur de la monnaie varie à chaque fois qu'on créée ou qu'on détruit de la monnaie. C'est ce qui fait peur à tous les économistes, et qu'on appelle l'inflation "nominale".
Et bien la monnaie libre résout ce problème ! Car si on exprime les prix en nombre de DU, alors la variation des prix n'est plus due à l'inflation nominale, mais uniquement à la variation réelle de la valeur des choses.

### Relativité de toute valeur
La valeur des choses, elle, dépend du fait que ces choses (biens ou services) correspondent plus ou moins bien à nos besoins, qui eux aussi varient en fonction des individus. Et oui, c'est une évidence que ce que qui a valeur pour une personne a une valeur différente pour d'autres. Et aussi, pour une même personne, ce qui a valeur à un moment donné n'aura pas la même valeur à ses yeux.

### Liberté de produire et estimer toute valeur
Cette relativité de toute valeur rend légitime toute personne ainsi que toutes les personnes d'une génération donnée, à estimer pour elle même ce qui a valeur ou non.
Comme tout le monde et toutes les générations créent la même part relative de monnaie, elle sont du coup à même d'utiliser leur part de monnaie pour financer et exercer des activités en accord avec leurs valeurs propres, sans subir le poids économique des autres ou des générations précédentes, ou le faire subir aux autres ou aux générations futures. Chacun-e reste libre !

### Démocratie
Avec la monnaie libre, le choix de création et l'injection de monnaie dans l'économie, pour financer une chose ou une autre, n'est plus le privilèges des banquiers ou des ministres (via la création monétaire qui se fait actuellement par crédit bancaire) mais elle est réalisée par chaque individu, via le DU qui est créé chaque jour (sans dette ni contre-partie de quelque nature que ce soit) sur son compte membre, et qu'il injecte lui-même dans l'économie quand il dépense ses Ğ1 nouvellement créées. Ce faisant, il vote pour et exprime ses valeurs, et influence lui même et directement la société dans laquelle il s’inscrit.

### Lien social
Pour garantir à toute la communauté que chaque personne ne va créer qu’une seule fois sa part de monnaie, une toile de confiance a été créée. Chaque nouveau membre de la communauté doit être connu et reconnu par 5 personnes qui sont déjà membres, qui se portent garantes de l’unicité du compte sur lequel est généré le DU quotidien du nouveau membre. Cette reconnaissance se faisant entre êtres humains réels, et non par des machines ou une entité centrale chargée d’émettre des documents d’identification, les gens sont amenés à se connaître et à se rencontrer, renforçant ainsi le lien social et les opportunités de faire des échanges.

### Cryptomonnaie et technologie blockchain
Imprimer des billets ou frapper des pièces est une manière complexe et coûteuse de gérer la monnaie. À l’heure actuelle, on ne sait pas le faire d’une façon qui soit suffisamment fiable, sécurisée et résiliente. Le contrôle de l’émission de la monnaie sur un support matériel est toujours centralisé, et peut donc être corrompu ou violé.
C’est pourquoi la Ğ1 est une cryptomonnaie, c’est à dire une monnaie numérique décentralisée et sécurisée par cryptographie, grâce à la technologie blockchain.
De plus, Duniter (DU-uniter), le logiciel qui génère le DU, qui gère la monnaie et la toile de confiance, est un logiciel sous licence libre : son code est ouvert, accessible, et vérifiable par tous.

### Sobriété énergétique
Les serveurs qui gèrent la monnaie tournent très bien sur des ordinateurs de taille très modeste du type RaspberryPi, un nano-ordinateur qui consomme (seulement) 5W d’électricité. Cela fait de la Ğ1 une monnaie low-tech, très sobre énergétiquement, et dont l’emprunte écologique est très faible (bien plus faible que celle de l’euro, et infiniment inférieure à celle du Bitcoin, où des fermes de calcul gigantesques sont en compétition les unes contre les autres et se disputent la création monétaire).