---
title: La Ğ1 June
description: La Ğ1...
---
## La première monnaie libre de l'histoire !

Le 8 mars 2017, est née la première monnaie libre de l'histoire de l'humanité.

Elle s'appelle « Ğ1 », ce qui se prononce « Ğ une » ou, plus simplement, « Jüne ».

Chaque jour, de plus en plus de gens :

* produisent leur quote-part de monnaie libre,* utilisent la Ğ1 comme intermédiaire d'échange pour acheter et vendre des biens ou services

## La Ğ1, c'est du pouvoir d'échange

Vous pouvez faire avec des Ğ1 le même genre d'échanges qu'avec d'autres monnaies.

La différence, c'est qu'il est plus facile de trouver clients et acquéreurs en Ğ1. Aucun membre n'a peur de manquer de Ğ1, car il sait très bien que, demain à midi, comme chaque jour à la même heure, il coproduira sa côte-part de monnaie.

C'est pourquoi un peu partout en France (et au-delà !), des humains s'échangent déjà :

* des biens d'occasion, *des coups de main,* des services professionnels,* des biens neufs, souvent produits de façon artisanale.

Pour poster leurs annonces, ils utilisent principalement [le site gchange.fr](https://www.gchange.fr/#/app/market/lg?last) ou l'[application ğchange pour Android](https://play.google.com/store/apps/details?id=fr.gchange).

Puisque la monnaie n'est pas monopolisée par un très petit nombre, les financements participatifs (*crowdfunding*) se font également beaucoup plus facilement.

## Plus de 2500 utilisateurs, en France, Belgique, et au-delà...

La plus forte concentration d'utilisateurs se trouve pour l'instant en Occitanie :

![](https://monnaie-libre.fr/wp-content/uploads/2020/06/carte-des-membres.png)[Voir la carte des membres de la Ğ1](https://demo.cesium.app/#/app/wot/map?c=46.6042:3.6475:6)

## Du pot de miel au violon : ce qu'on échange en Ğ1

Ce que vous pouvez échanger en Ğ1 au niveau local sera très variable en fonction de l'endroit où vous vous trouvez.

Par exemple, l'économie Ğ1 est pour l'instant plus dynamique en Occitanie que dans le reste de la France.

Jusqu'à présent, les utilisateurs de la Ğ1 ont déjà échangé un certain nombre de biens et de services :

<iframe title="Ce qu'ils ont acheté ou vendu avec leurs Ğ1" width="800" height="450" src="https://www.youtube.com/embed/0bE9QzTb7SE?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

Vous retrouverez l'intégralité des vidéos sur la chaîne [Ma Monnaie Libre](https://www.youtube.com/channel/UCo0A8va0HUe98r5pIxWmIMg/videos).

Vous pouvez aussi regarder une des vidéos dans lesquelles un utilisateur (Pi) raconte son expérience, avec beaucoup d'humour :

<iframe title="[RML12] Mon expérience utilisateur 1 an et 1/2 d'échanges avec la Ǧ1 - Pi Nguyen" width="800" height="450" src="https://www.youtube.com/embed/08zh5gw8wr0?start=1722&amp;feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

## Une monnaie complètement sécurisée, grâce à la technologie de la blockchain

La notion de monnaie libre en elle-même est indépendante du support : une monnaie libre peut être matérialisée physiquement (par des pièces ou des billets), tout comme elle peut être 100% numérique.

Pour la 1ère monnaie libre de l'histoire de l'humanité, la Ğ1, le choix a été fait, de faire une monnaie numérique, en utilisant la technologie de la blockchain, pour la sécurité maximale qu'elle offre.

À la base de la monnaie, il n'y a donc pas de pièce ni de billet.

Après, il est tout à fait possible techniquement de créer des sortes de "billets" pour des Ğ1.

Mais cela pose un problème de confiance envers la personne ou l'entité qui émet lesdits billets.

Des projets sont en cours d'expérimentation, mais il faut toujours garder en tête qu'en Ğ1, la "vraie monnaie", ce sont les nombres dans la blockchain. 

Notez que c'est exactement l'inverse pour des monnaies dettes comme l'Euro, dans lesquelles la "vraie monnaie" est la masse de billets et de pièces, tandis que les nombres sur votre compte en banque ne sont que la promesse que vous fait la banque de vous donner cette vraie monnaie lorsque vous la lui demandez.

## Allez-y mollo !

La Ğ1 est un projet récent (elle est née en mars 2017).

Considérez la Ğ1 comme une expérimentation grandeur nature.

Ne mettez pas tous vos espoirs dans la Ğ1.

Il est possible que vous trouviez quelques avantages à utiliser la Ğ1, mais en aucun cas la Ğ1 ne résoudra tous vos problèmes.

## Comment recevoir et envoyer des Ğ1

Pour recevoir et envoyer des Ğ1, il vous faudra utiliser une application cliente.

La plus facile d'utilisation est [Cesium](https://cesium.app/) :
[![](https://monnaie-libre.fr/wp-content/uploads/2019/03/cesium-g1.png)](https://cesium.app/)

Rendez-vous sur le site [cesium.app](https://cesium.app/) pour télécharger la version de Cesium qui convient à votre système d'exploitation.

Une fois votre compte "créé", vous obtiendrez une "clef publique", sorte de RIB que vous pouvez transmettre à toute personne qui souhaiterait vous envoyer des Ğ1.

## Comment vendre des choses en Ğ1

Il existe plusieurs plate-formes sur lesquelles vous pouvez trouver biens et services à échanger contre des Ğ1 :

* [ğchange](https://www.gchange.fr/#/app/market/lg?last) : plate-forme la plus dynamique actuellement, spécifiques à la Ğ1* [Communecter](https://www.communecter.org/#annonces?devise=%C4%9E1), plate-forme non spécifique à la Ğ1 mais sur laquelle il est possible de filtrer les annonces par devise

## Comment devenir coproducteur de la Ğ1

Pour prendre part à la création monétaire, il faudra transformer votre compte porte-feuille en compte membre :

![Transformer un compte portefeuille en compte membre](https://monnaie-libre.fr/wp-content/uploads/2019/01/transformer-en-compte-membre-1024x576.png)

Pour cela, vous devez être certifié par au moins 5 personnes déjà membres, en respectant [la licence d'utilisation ğ1](https://duniter.org/fr/wiki/licence-g1/), c'est-à-dire que ces 5 personnes certifient que :

1. vous existez,2. elles vous connaissent suffisamment bien,3. elles ont plusieurs moyens de vous contacter, 4. elles connaissent plusieurs personnes capables de vous contacter, 5. vous avez généré le document de révocation qui vous permet de désactiver votre compte membre s'il est piraté (vous n'avez pas besoin de leur transmettre ledit document ceci dit)

Vous devrez obtenir vos 5 certifications dans un délai maximum de 2 mois après avoir fait votre demande de compte membre.

Assurez-vous donc de rencontrer des personnes déjà 
membres, et de nouer des relations de confiance afin qu'elles acceptent 
de vous certifier.

Pour rencontrer des personnes déjà membres, consultez [la carte des membres](https://g1.duniter.fr/#/app/wot/map) et prenez contact avec votre groupe local via [le forum](https://forum.monnaie-libre.fr/).

### Pourquoi votre compte doit être certifié par 5 personnes

C'est simplement comme ça que fonctionne la **toile de confiance**.
![](https://monnaie-libre.fr/wp-content/uploads/2020/06/carte-toile-de-confiance-G1-mondiale.png)[Voir la toile de confiance Ğ1](https://zettascript.org/tux/g1/worldwotmap.html)

La toile de confiance est ce qui garantit la sécurité du système : elle permet d'empêcher la fraude, en s'assurant qu'aucun être humain ne se fait faux-monnayeur en possédant plus d'un compte. 

Elle permet surtout d'empêcher des pirates de multiplier les comptes membres afin de produire plus de monnaie que d'autres, ce qui ferait perdre toute confiance dans la monnaie.

Notez que ce mécanisme est propre à la Ğ1 : c'est le choix qu'on fait les développeurs pour permettre d'identifier de façon unique les utilisateurs, sans avoir besoin de recourir aux cartes d'identité ou aux passeports des États. Il serait tout à fait possible de faire des monnaies libres dans lesquelles l'identification aurait lieu par d'autres mécanismes.

## Comment rencontrer des utilisateurs

Plusieurs solutions s'offrent à vous : 

### Solution n°1 : Contacter votre groupe local

Il arrive souvent que les membres s'organisent sous forme de "groupe local".

Un "groupe local" est simplement un groupe d'être humains qui choisissent, de leur propre chef, de se doter d'un ou de plusieurs outil(s) pour communiquer entre eux et organiser des rencontres.

Le moyen de communication choisi varie d'un groupe à l'autre ; il peut s'agir : 

* d'un groupe de **discussion** par e-mail ("*mailing list*") *d'un groupe Facebook* d'un canal Discord *d'un chat* d'un forum

Aidez-vous de la carte ci-dessous pour trouver le groupe les plus proche de chez vous et découvrir quels canaux ce dernier utilise pour communiquer :

<iframe width="100%" height="480px" frameborder="0" src="https://framacarte.org/fr/map/duniter-g1_8702?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=true&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=null&amp;onLoadPanel=none&amp;captionBar=false&amp;datalayers=13342&amp;fullscreenControl=false#5/45.660/4.351" scrolling="yes" class="iframe-class"></iframe>

[Voir la carte des groupes en grand](https://framacarte.org/fr/map/groupes-locaux-et-rencontres-g1_8702)

### Solution n°2 : Contacter directement les membres qui habitent près de chez vous

L'application cliente Cesium dispose d'une surcouche qui la dote d'un annuaire, qui vous permet de contacter directement les membres qui habitent près de chez vous.

Dans Cesium, rendez-vous dans l'onglet Annuaire puis dans Carte. 

Pour plus de confort d'utilisation, faites-le plutôt sur ordinateur que sur mobile

Vous pouvez aussi consulter la carte en ligne (mais vous ne pourrez pas vous connecter à votre compte pour envoyer un message) :

[Voir la carte des utilisateurs de la Ğ1](https://demo.cesium.app/#/app/wot/map?c=46.6042:3.6475:6)