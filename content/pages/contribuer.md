---
title: Contribuer
description: Toutes les informations pour participer !
---

## Ils changent le monde

On entend parfois certains <lexique>jünistes</lexique> dire que la Ğ1 est "_une œuvre d'art collective_".

Et c'est vrai qu'il y a beaucoup de gens qui font des choses autour des monnaies libres en général ou de la Ğ1 en particulier.

C'est tout une symphonie qui se met en place, sans que personne sache trop comment.

Si la Ğ1 existe aujourd'hui, c'est grâce à tous ces gens :

Ceux qui développent les logiciels : vous trouverez leurs noms sur le site de [Duniter][0], le moteur de la blockchain.

Ceux qui valident les transactions de la blockchain en [forgeant des blocs][1].

Ceux traduisent les logiciels : **Scan Le Gentil**, **Vivakvo**...

Ceux qui rédigent de la documentation.

Ceux qui rédigent des rapports de bug.

Ceux qui développent l'économie : ce sont tous ceux qui, [par leurs échanges][2], valorisent la Ğ1, et prouvent au quotidien qu'une monnaie libre permet d'échanger.

Ceux qui en parlent autour d'eux.

Ceux qui créent des groupes locaux.

Ceux qui organisent des parties de [Ğeconomicus][3].

Ceux qui créent des [mèmes rigolos][4] : **Rimek**, **Attilax** et d'autres...

Ceux qui donnent des conférences pour expliquer la création monétaire :

- **Pi** pour sa conférence humoristique [Mon expérience utilisateur 1 an et 1/2 d'échanges avec la Ǧ1][5]_ **Martino** pour [sa conférence en anglais à Berlin][6]_ **Julien Rousseau** pour sa conférence [Monnaie libre et collapsologie][7]\* **POUHIOU** et **Squeek** pour leur pièce de théâtre [La monnaie libre quand on y connaît que pouic][8]

Ceux qui créent du contenu pédagogique en ligne :

- **David Chazaviel**, notamment pour sa [TRM pour les enfants][9]_ **Attilax**, pour [Ma monnaie libre][10], sa série vidéo qui demande à des jünistes : "*C'est quoi la monnaie libre ?*"._ **Le Brice**, pour [sa chaîne qui parle de monnaie libre][11].

Ceux qui écrivent des livres, comme **Denis La Plume**, notamment pour son ouvrage [La monnaie : ce qu'on ignore][12].

Ceux qui font de la création graphique :

- **Thomas "DiG" Di Gregorio** pour le logo de la Ğ1 et celui des monnaies libres

**poka**, qui entre autres choses, met à disposition des infrastructures permettant d'héberger des contenus.

## A vous de jouer !

La Ğ1 n'appartient à personne.

D'ailleurs, si on vous a dit que ce site était le "_site officiel_" des monnaies libres ou de la Ğ1, alors mettons ça au clair toute de suite : ni la monnaie libre ni la Ğ1 ne peuvent avoir de "_site officiel_", car il n'y a pas d'autorité qui les gouverne.

Certes, ce site est souvent celui qui sort en premier dans les moteurs de recherche lorsque vous tapez "_monnaie libre_", mais d'autres sites parlent des monnaies libres ou de la Ğ1 et il ne sont pas moins légitimes que celui-ci pour le faire.

Comme "gilet jaune", certaines personnes peuvent essayer déposer la marque "monnaie libre", mais ils se feront probablement retoquer, car "monnaie libre" n'est que l'association de deux noms communs.

La Ğ1 n'a pas de tête.

La Ğ1 n'a pas de centre.

Vous êtes donc libre de contribuer comme bon vous semble, sans attendre que qui que ce soit vous en donne l'autorisation.

Si vous souhaitez collaborer avec d'autres gens sur certains projets, vous pouvez vous manifester sur n'importe quelle plateforme ou réseau sur lequel se trouvent des jünistes : les jünistes saurons en général assez bien vous mettre en relation avec d'autres jünistes pour que vous puissiez joindre vos efforts 😉

[0]: https://duniter.org/fr/
[1]: https://duniter.org/fr/miner-des-blocs/
[2]: https://www.gchange.fr/#/app/market/lg?last
[3]: http://geconomicus.glibre.org/
[4]: https://axiom-team.fr/memes/
[5]: https://www.youtube.com/watch?v=08zh5gw8wr0
[6]: https://www.youtube.com/watch?v=7LqdI7pALBE
[7]: https://www.youtube.com/watch?v=Ek4Msa_uAQY
[8]: https://www.youtube.com/watch?v=EP41sx2D6BY
[9]: http://cuckooland.free.fr/LaTrmPourLesEnfants.html
[10]: https://www.mamonnaielibre.fr/
[11]: https://www.youtube.com/channel/UCU_Yip6I_axwjTNiswSGokQ/videos?view=0&sort=p&flow=grid
[12]: https://blog.denislaplume.fr/2017/09/29/le-livre-la-monnaie-ce-quon-ignore-est-sorti/
