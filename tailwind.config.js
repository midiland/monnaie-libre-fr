/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

// cat node_modules/tailwindcss/stubs/defaultConfig.stub.js

import colors from 'tailwindcss/colors'
import defaultTheme from 'tailwindcss/defaultTheme'

// Delete the 2xl breakpoint from the object
const containerScreens = Object.assign({}, defaultTheme.screens)
delete containerScreens['2xl']

module.exports = {
  theme: {
    // compatible with @nuxtjs/color-mode
    darkSelector: '.dark',
    extend: {
      colors: {
        dark: '#222222',
        hover: {
          light: colors.indigo[200],
          DEFAULT: colors.indigo[600],
          dark: colors.indigo[900],
        },
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            maxWidth: '80ch',
          },
        },
        dark: {
          css: {
            color: theme('colors.gray.200'),
            a: {
              color: theme('colors.blue.100'),
              '&:hover': {
                color: theme('colors.blue.200'),
              },
            },
            strong: {
              color: theme('colors.gray.100'),
            },
            'ol > li::before': {
              color: theme('colors.gray.400'),
            },
            'ul > li::before': {
              backgroundColor: theme('colors.gray.600'),
            },
            hr: {
              borderColor: theme('colors.gray.600'),
            },
            blockquote: {
              color: theme('colors.gray.200'),
              borderLeftColor: theme('colors.gray.600'),
            },
            h1: {
              color: theme('colors.gray.100'),
            },
            h2: {
              color: theme('colors.gray.100'),
            },
            h3: {
              color: theme('colors.gray.100'),
            },
            h4: {
              color: theme('colors.gray.100'),
            },
            'figure figcaption': {
              color: theme('colors.gray.400'),
            },
            code: {
              color: theme('colors.gray.100'),
            },
            'a code': {
              color: theme('colors.gray.100'),
            },
            pre: {
              color: theme('colors.gray.200'),
              backgroundColor: theme('colors.gray.800'),
            },
            thead: {
              color: theme('colors.gray.100'),
              borderBottomColor: theme('colors.gray.400'),
            },
            'tbody tr': {
              borderBottomColor: theme('colors.gray.600'),
            },
          },
        },
      }),
      zIndex: {
        '-1': '-1',
        '-10': '-10',
      },
    },
    container: {
      screens: containerScreens,
      center: true,
    },
  },
  variants: {
    margin: ['responsive', 'last'],
    backgroundColor: [
      'responsive',
      'dark',
      'group-hover',
      'focus-within',
      'hover',
      'focus',
      'dark-hover',
      'dark-group-hover',
      'dark-even',
      'dark-odd',
    ],
    borderColor: [
      'responsive',
      'dark',
      'group-hover',
      'focus-within',
      'hover',
      'focus',
      'dark-focus',
      'dark-focus-within',
    ],
    textColor: [
      'responsive',
      'dark',
      'group-hover',
      'focus-within',
      'hover',
      'focus',
      'dark-hover',
      'dark-active',
    ],
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled'],
      typography: ['dark'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('tailwindcss-dark-mode')(),
    require('tailwindcss-debug-screens'),
  ],
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
      'content/**/*',
      './node_modules/tailwindcss-dark-mode/prefers-dark.js',
    ],
  },
}
