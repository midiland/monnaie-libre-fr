# TODO

## A installer
- Statistiques open source node.js/graphql : https://github.com/electerious/Ackee
- Flux rss/atom : https://content.nuxtjs.org/fr/advanced#int%C3%A9gration-avec-nuxtjsfeed

## A corriger quand ce sera fixé
- Remove postcss 7 when nuxt/tailwindcss will be updated (See [here](https://tailwindcss.nuxtjs.org/setup#tailwind-2))
- Remove `resolutions` in package.json (See [here](https://github.com/nuxt/nuxt.js/issues/8639#issuecomment-767552372))
